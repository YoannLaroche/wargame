package wargame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FenetreJeu extends JFrame implements IConfig{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5187771421944500579L;
	
	PanneauMenu menuPanel;
	PanneauJeu panel;
	Carte loadCarte ;
	boolean tourJ=true;
	
	public FenetreJeu(int fen) {
		setResizable(false);
		/* MENU */
		if(fen == MENU) {
			JPanel menuPanel = new JPanel();
			
			try {
				menuPanel = new PanneauMenu();
			} catch (IOException e1) {
				menuPanel.setBackground(Color.DARK_GRAY);
				System.out.println("Probelme image menu");
			}
			
			menuPanel.setLayout(null);
			
			/* Bouton de nouvelle partie */
			JButton newGame = new JButton("Nouvelle Partie");
			newGame.setBounds(POSITIONX, POSITIONY+LOGOY+ECART, DIMX, DIMY);
			newGame.setFocusPainted(false);
			newGame.setForeground(Color.white);
			newGame.setBackground(Color.DARK_GRAY);
			
			newGame.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					@SuppressWarnings("unused")
					FenetreJeu frame = new FenetreJeu("Wargame", JEU);
					setVisible(false);	
					dispose();
				}
			});
			
			/* Bouton charger partie */
			JButton loadGame = new JButton("Charger partie");
			loadGame.setBounds(POSITIONX,(int)(newGame.getLocation().getY())+ECART+DIMY, DIMX, DIMY);
			loadGame.setFocusPainted(false);
			loadGame.setForeground(Color.white);
			loadGame.setBackground(Color.DARK_GRAY);
			
			loadGame.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						FileInputStream fis;
						try {

							JFileChooser charge = new JFileChooser(".");
							File fichier;				
							charge.showOpenDialog(null);
							if((fichier = charge.getSelectedFile())!=null) {
								fis = new FileInputStream(fichier);
								ObjectInputStream ois = new ObjectInputStream(fis);
								loadCarte=(Carte)ois.readObject();	
								ois.close();	
																
								FenetreJeu jeu = new FenetreJeu("Wargame", JEU);
								jeu.panel.carte=loadCarte;
								Heros.nbHeros=jeu.panel.carte.getNbHerosCarte();
								Monstre.nbMonstre=jeu.panel.carte.getNbMonstreCarte();
								setVisible(false);	
								dispose();	
							}
							
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							//e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							//e1.printStackTrace();
						} catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							//e1.printStackTrace();
						}
				}
			});
			
			/* Bouton Quitter */
			JButton exit = new JButton("Quitter");
			exit.setBounds(POSITIONX,(int)(loadGame.getLocation().getY())+ECART+DIMY, DIMX, DIMY);
			exit.setFocusPainted(false);
			exit.setForeground(Color.white);
			exit.setBackground(Color.DARK_GRAY);
			exit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);	
					dispose();
					System.exit(0);
				}
			});			
			menuPanel.add(newGame);
			menuPanel.add(loadGame);
			menuPanel.add(exit);

			
			Dimension tailleEcran = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			int hauteur = (int)tailleEcran.getHeight();
			int largeur = (int)tailleEcran.getWidth();
			
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setMinimumSize(new Dimension(POSITIONX*2+DIMX,DIMY*3+(POSITIONY+ECART)*2+LOGOY+JESAISPAS));
			setLocation(largeur/2-getWidth()/2,hauteur/2-getHeight()/2);
			setVisible(true);
			
			getContentPane().add(menuPanel);
		}
		
		if(fen == JEU){
			JPanel decalage = new JPanel();
			decalage.setSize(0, 36);

			Hud menu = new Hud();
			panel = new PanneauJeu();
			panel.menu = menu;
		
			//Boutton	
			JButton bSave=new JButton("Sauvegarder");
			bSave.setFocusPainted(false);
			bSave.setForeground(Color.white);
			bSave.setBackground(Color.DARK_GRAY);
			JButton bRest=new JButton("Charger");
			bRest.setFocusPainted(false);
			bRest.setForeground(Color.white);
			bRest.setBackground(Color.DARK_GRAY);
			JButton bFinTour=new JButton(new ImageIcon(this.getClass().getResource("img/epee.png")));
			bFinTour.setContentAreaFilled(false);
			bFinTour.setBorderPainted(false);
			JButton bMenu = new JButton("Menu");
			bMenu.setFocusPainted(false);
			bMenu.setForeground(Color.white);
			bMenu.setBackground(Color.DARK_GRAY);
			bFinTour.setFocusPainted(false);
			bFinTour.setForeground(Color.white);
			bFinTour.setBackground(Color.DARK_GRAY);
		
		
		
			// JFrame
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setMinimumSize(new Dimension (panel.getWidth() + ZONE_AFFICHAGE, panel.getHeight() + decalage.getHeight()));
			this.setResizable(false);
			setLayout(null);

			bFinTour.addMouseListener(new MouseAdapter() {
				public void mouseEntered(MouseEvent e) {
					bFinTour.setBorderPainted(true);
				}
				public void mouseExited(MouseEvent e) {
					bFinTour.setBorderPainted(false);
				}
			});
		
			bFinTour.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
									
					for(int y=0; y<HAUTEUR_CARTE; y++) {
						for(int x=0; x<LARGEUR_CARTE; x++) {
							if(panel.carte.plateau[y][x] instanceof Heros && ((Heros)panel.carte.plateau[y][x]).getAJoue()!=true && ((Heros)panel.carte.plateau[y][x]).getPoints() < ((Heros)panel.carte.plateau[y][x]).getSoldat().getPoints()) {
								((Soldat)panel.carte.plateau[y][x]).setPoints(((Soldat)panel.carte.plateau[y][x]).getPoints()+1);
								PanneauJeu.BOIRE.jouerSon();
							}
						}
					}
					
					for(int y=0; y<HAUTEUR_CARTE; y++) {
						for(int x=0; x<LARGEUR_CARTE; x++) {
							if(panel.carte.plateau[y][x] instanceof Monstre && ((Monstre)panel.carte.plateau[y][x]).getAJoue()!=true) {
								((Monstre)panel.carte.plateau[y][x]).tourMonstre(panel.carte);
							}
	
						}
					}
					
					panel.carte.setTour(panel.carte.getTour()+1);
					tourJ=false;
					panel.carte.resetTour();
					menu.soldat=null;
					menu.nom.setVisible(false);
					menu.pv.setVisible(false);
					menu.puissance.setVisible(false);
					menu.tir.setVisible(false);
					repaint();
					menu.repaint();
				}
			});
			
			bSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						
						JFileChooser save = new JFileChooser(".");
						PrintWriter sortie = null;
						File fic;
	
						if (save.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
							fic = save.getSelectedFile();
							try {
								sortie = new PrintWriter(new FileWriter(fic.getPath(), true));
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							FileOutputStream fichier = new FileOutputStream(fic);
							ObjectOutputStream OOS=new ObjectOutputStream(fichier);
							panel.carte.setNbHerosCarte(Heros.nbHeros);
							panel.carte.setNbMonstreCarte(Monstre.nbMonstre);
							OOS.writeObject(panel.carte);
							OOS.flush();
							OOS.close();
							sortie.close();
						}
						
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
					}
					
				}
			});	
			
			bRest.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					FileInputStream fis;
					try {
	
						JFileChooser charge = new JFileChooser(".");
						File fichier;
						
						charge.showOpenDialog(null);
						if((fichier = charge.getSelectedFile())!=null) {
							fis = new FileInputStream(fichier);
							ObjectInputStream ois = new ObjectInputStream(fis);
							panel.carte=(Carte)ois.readObject();
							Heros.nbHeros=panel.carte.getNbHerosCarte();
							Monstre.nbMonstre=panel.carte.getNbMonstreCarte();
							ois.close();
						}
						
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
					}
					 repaint();
					
					
			}});
			
			bMenu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					new FenetreJeu("Wargame", MENU);
				}
			});
			bMenu.setBounds(20,HAUTEUR_CARTE*NB_PIX_CASE - 330, ZONE_AFFICHAGE - 40, 100);
			menu.add(bMenu);
			
			bSave.setBounds(20,HAUTEUR_CARTE*NB_PIX_CASE - 220, ZONE_AFFICHAGE - 40, 100);
			menu.add(bSave);
			
			bRest.setBounds(20, HAUTEUR_CARTE*NB_PIX_CASE - 110, ZONE_AFFICHAGE - 40, 100);
			menu.add(bRest);
			
			bFinTour.setBounds(20, 20, ZONE_AFFICHAGE - 40, 100);
			menu.add(bFinTour);
			
			add(panel);
			panel.setLocation(0,0);
			add(menu);
			menu.setBounds(LARGEUR_CARTE*NB_PIX_CASE, 0,ZONE_AFFICHAGE,HAUTEUR_CARTE*NB_PIX_CASE);
			setVisible(true);
			pack();
			System.out.println(panel.getHeight());
			System.out.println(panel.getWidth());
		}
	}
	
	public FenetreJeu(String nom, int fen) {
		this(fen);
		this.setTitle(nom);
	}
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		FenetreJeu frame = new FenetreJeu("Wargame",MENU);


	}
}
