package wargame;

public class Soldat extends Element implements ISoldat {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean aJoue=false;
	
	private int points;
	private int portee;
	private int puissance;
	private int tir;
	private int tour;
	
	public Soldat(Position position, int points, int portee, int puissance, int tir, int tour) {
		super(position);
		this.points = points;
		this.portee = portee;
		this.puissance = puissance;
		this.tir = tir;
		this.tour = tour;
	}

	public int getPoints() {
		return points;
	}
	public void setPoints(int point) {
		this.points = point;}
	
	public int getPortee() {
		return portee;
	}

	public int getPuissance() {
		return puissance;
	}
	
	public int getTir() {
		return tir;
	}
	
	public int getTour() {
		return tour;
	}
	
	public boolean getAJoue() {
		return aJoue;
	}
	
	public void setAJoue(boolean b) {
		this.aJoue=b;
	}
	
	public void tourSuivant() {this.tour++;}
			
	public void combat(Soldat soldat) {
		int degats;
		System.out.println("Position heros = " + getPosition().getX()/NB_PIX_CASE + " "+ getPosition().getY()/NB_PIX_CASE + " " + soldat.getPosition().getX()/NB_PIX_CASE + " " + soldat.getPosition().getY()/NB_PIX_CASE);
		if (getPosition().estVoisine(soldat.getPosition())) {
			degats = (int)(Math.random()*puissance+1);
			System.out.println("Degat 1 " + degats);
			soldat.points -= degats;
			if (soldat.points > 0) {
				System.out.println("Degat 2 :" + degats);
				degats = (int)(Math.random()*soldat.puissance+1);
				points -= degats;
			}
			
			if(this instanceof Heros) {
				PanneauJeu.HEROSATTAQUE.jouerSon();
			}else if(this instanceof Monstre) {
				PanneauJeu.MONSTREATTAQUE.jouerSon();
			}
		}
		else {
			degats = (int)(Math.random()*tir+1);
			soldat.points -= degats;
			if (soldat.points > 0 && (Math.abs( getPosition().getX()/NB_PIX_CASE - soldat.getPosition().getX()/NB_PIX_CASE ) <= soldat.getPortee() && Math.abs(getPosition().getY()/NB_PIX_CASE - soldat.getPosition().getY()/NB_PIX_CASE ) <= (soldat.getPortee()))) {
				degats = (int)(Math.random()*soldat.tir+1);
				points -= degats;
			}
			
			
			if(this instanceof Heros) {
				PanneauJeu.HEROSFLECHE.jouerSon();
			}else if(this instanceof Monstre) {
				PanneauJeu.MONSTREFLECHE.jouerSon();
			}
		
		}
	}

	
}
