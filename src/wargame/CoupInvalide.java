package wargame;

public class CoupInvalide extends Exception {

	private static final long serialVersionUID = 1L;
	public final String nom;
	
	public CoupInvalide (String str) {
		super("Le coup " + str + "n'est pas valide");
		nom = str;
	}

}
