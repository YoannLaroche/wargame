package wargame;

import java.awt.Graphics;

import java.awt.Image;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

import wargame.ISoldat.TypesH;
import wargame.ISoldat.TypesM;

public class Carte extends CoupInvalide implements IConfig, ICarte,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Element[][] plateau = new Element[HAUTEUR_CARTE][LARGEUR_CARTE];
	
	protected static Image NAIN,ELF,HOBBIT,HUMAIN,ORC,GOBELIN,TROLL,NAIN_NB,ELF_NB,HOBBIT_NB,HUMAIN_NB,ROCHER,FORET,EAU, BROUILLARD, HERBE; 
	
	private int tour;
	
	private int nbHerosCarte;
	
	private int nbMonstreCarte;
	
	public Carte() {
		super("");
		setTour(0);
		Heros.nbHeros=0;
		Monstre.nbMonstre=0;
		
		try {
			NAIN = ImageIO.read(this.getClass().getResource("img/nain.png"));
			HUMAIN = ImageIO.read(this.getClass().getResource("img/humain.png"));
			ELF = ImageIO.read(this.getClass().getResource("img/elf.png"));
			HOBBIT = ImageIO.read(this.getClass().getResource("img/hobbit.png"));

			NAIN_NB = ImageIO.read(this.getClass().getResource("img/nainNB.png"));
			HUMAIN_NB = ImageIO.read(this.getClass().getResource("img/humainNB.png"));
			ELF_NB = ImageIO.read(this.getClass().getResource("img/elfNB.png"));
			HOBBIT_NB = ImageIO.read(this.getClass().getResource("img/hobbitNB.png"));

			ORC = ImageIO.read(this.getClass().getResource("img/orc.png"));
			GOBELIN = ImageIO.read(this.getClass().getResource("img/gobelin.png"));
			TROLL = ImageIO.read(this.getClass().getResource("img/troll.png"));

			HERBE = ImageIO.read(this.getClass().getResource("img/herbe.png"));
			BROUILLARD = ImageIO.read(this.getClass().getResource("img/brouillard.png"));

			FORET = ImageIO.read(this.getClass().getResource("img/foret.png"));
			ROCHER = ImageIO.read(this.getClass().getResource("img/rocher.png"));
			EAU = ImageIO.read(this.getClass().getResource("img/eau.png"));
		}catch(IOException e) {
				e.printStackTrace();
		}
		int x,y;
		Position position = new Position(0,0);
		for(y=0;y<HAUTEUR_CARTE;y++) {
			for(x=0;x<LARGEUR_CARTE;x++) {
				position.setY(y*NB_PIX_CASE);
				position.setX(x*NB_PIX_CASE);
				plateau[y][x] = new Element(position);
			}
		}
		
		int nbObstacles = 0;
		while (nbObstacles < NB_OBSTACLES) {
			x = (int)(Math.random()*(LARGEUR_CARTE ));
			y = (int)(Math.random()*(HAUTEUR_CARTE ));
			if (!plateau[y][x].getEtat()){ 
				plateau[y][x] = new Obstacle(Obstacle.TypeObstacle.getObstacleAlea(),new Position(x*NB_PIX_CASE,y*NB_PIX_CASE));
				plateau[y][x].setEtat(true);
				nbObstacles++;
			}
		}
		int nbHeros = 0;
		while (nbHeros < NB_HEROS) {
			x = (int)(Math.random()*(LARGEUR_CARTE/2 ));
			y = (int)(Math.random()*(HAUTEUR_CARTE ));
			if (!plateau[y][x].getEtat()){ 
				plateau[y][x] = new Heros (new Position(x*NB_PIX_CASE,y*NB_PIX_CASE), TypesH.getTypeHAlea());
				plateau[y][x].setEtat(true);
				nbHeros++;
			}
		}
		
		int nbMonstres = 0;
		while (nbMonstres < NB_MONSTRES) {
			x = (int)(Math.random()*(LARGEUR_CARTE/2) + LARGEUR_CARTE/2);
			y = (int)(Math.random()*(HAUTEUR_CARTE ));
			if (!plateau[y][x].getEtat()){ 
				plateau[y][x] = new Monstre (new Position(x*NB_PIX_CASE,y*NB_PIX_CASE), TypesM.getTypeMAlea());
				plateau[y][x].setEtat(true);
				nbMonstres++;
			}
		}
	}
		
	public int getNbHerosCarte() {
		return nbHerosCarte;
	}

	public void setNbHerosCarte(int nbHerosCarte) {
		this.nbHerosCarte = nbHerosCarte;
	}

	public int getNbMonstreCarte() {
		return nbMonstreCarte;
	}

	public void setNbMonstreCarte(int nbMonstreCarte) {
		this.nbMonstreCarte = nbMonstreCarte;
	}

	public Element getElement(Position pos) {
			return plateau[pos.getY()][pos.getX()];
	}
	
	//
	public Element affecterPosition(Element elem, Position pos) throws CoupInvalide{		
		if ((plateau[pos.getY()][pos.getX()].getEtat() && plateau[pos.getY()][pos.getX()] instanceof Monstre  && Math.abs(pos.getX()-elem.getPosition().getX()/NB_PIX_CASE ) <= ((Heros)elem).getPortee() && Math.abs(pos.getY()-elem.getPosition().getY()/NB_PIX_CASE ) <= ((Heros)elem).getPortee()) || (( Math.abs(pos.getX()-elem.getPosition().getX()/NB_PIX_CASE ) <= 1 && Math.abs(pos.getY()-elem.getPosition().getY()/NB_PIX_CASE ) <= 1 ) && (!plateau[pos.getY()][pos.getX()].getEtat() || plateau[pos.getY()][pos.getX()].getPosition().equals(elem.getPosition())))) {//ok
			return new Element (new Position(pos.getX()*NB_PIX_CASE,pos.getY()*NB_PIX_CASE));//On met a jour la position montrer quand on veut deplacer le heros
		}
		throw new CoupInvalide(""+pos);
	}
	
	
	public Heros trouveHeros(Position pos) {
		Heros h = null;
		int herosPresent = 0;
		int i = 0;
		int j = 0;
		Heros[] tabH= new Heros[30];
		
		// calcule de l'indice de la position de la position du monstre
		int posMonstreX = (int)(pos.getX()/NB_PIX_CASE);
		int posMonstreY = (int)(pos.getY()/NB_PIX_CASE);
		//System.out.println("pos getX = "+pos.getX()+" pos getY "+pos.getY());
		//System.out.println("posMonstreX = "+posMonstreX+" posMonstreY "+posMonstreY);
		int portee = 0;
		
		// calcule intervalle portee
		if(this.plateau[posMonstreY][posMonstreX] instanceof Monstre) {
			portee = ((Monstre)this.plateau[posMonstreY][posMonstreX]).getPortee();
		}
	
		int porteeMinX = posMonstreX - portee;
		int porteeMaxX = posMonstreX + portee;
		int porteeMinY = posMonstreY - portee;
		int porteeMaxY = posMonstreY + portee;
				
		//System.out.println("portee = "+portee+" PorteeMinX = "+porteeMinX+" PorteeMaxX = "+porteeMaxX+" PorteeMinY = "+porteeMinY+" PorteMaxY "+porteeMaxY);
		
		
		/* Determine si un hero est a porter*/
		for(i = porteeMinX ; i<= porteeMaxX; i++) {
			for(j = porteeMinY; j<= porteeMaxY; j++) {
				if(i>=0 && i<LARGEUR_CARTE && j>=0 && j<HAUTEUR_CARTE /*&& i != posMonstreX && j != posMonstreY*/) {
				//	System.out.println("verifie en "+i+" "+j);
					if(this.plateau[j][i] instanceof Heros) {
					//	System.out.println("HERO PRESENT EN "+i+" "+j);
						tabH[herosPresent]=(Heros)this.plateau[j][i];
						herosPresent++;
					}
				}
			}
		}
		//System.out.println(herosPresent);
		/* choisie un hero au hasard
		   Ameliorable avec un tableau de position*/
		if(herosPresent>0) {
			return tabH[(int)(Math.random()*herosPresent)];
		}
		
		return h;
	}
	
	public void mort(Soldat perso) {
		if(perso instanceof Heros) {
			Heros.nbHeros--;
		}
		if(perso instanceof Monstre) {
			Monstre.nbMonstre--;
		}
		plateau[perso.getPosition().getY()/NB_PIX_CASE][perso.getPosition().getX()/NB_PIX_CASE]=new Element(perso.getPosition());
	}
	
			
	public void toutDessiner(Graphics g) { 
		int i,j,k,l;
		for(i=0;i<HAUTEUR_CARTE;i++) { 
			for(j=0;j<LARGEUR_CARTE;j++) { 
				plateau[i][j].seDessinerCache(g); 
			} 			
		} 		
		
		for(i=0;i<HAUTEUR_CARTE;i++) {
			for(j=0;j<LARGEUR_CARTE;j++) { 
				if (plateau[i][j] instanceof Heros) { 
					for(k = i - ((Soldat)(plateau[i][j])).getPortee(); k <= i + ((Soldat)(plateau[i][j])).getPortee(); k++) {
						for(l = j - ((Soldat)(plateau[i][j])).getPortee(); l <= j + ((Soldat)(plateau[i][j])).getPortee(); l++) { 	
							if ( k > -1 && k < HAUTEUR_CARTE && l > -1 && l < LARGEUR_CARTE) {
								plateau[k][l].seDessiner(g); 
							}
						} 					
					}						
					plateau[i][j].seDessiner(g); 
				} 	
			}
		}
	}
	
	public void resetTour() {
		for(int i=0;i<HAUTEUR_CARTE;i++) {
			for(int j=0;j<LARGEUR_CARTE;j++) { 
				if (this.plateau[i][j] instanceof Soldat) { 
					((Soldat)this.plateau[i][j]).setAJoue(false);
					
					/*if (this.plateau[i][j] instanceof Heros) { 
						switch(((Heros)plateau[i][j]).getSoldat()) {
						case NAIN:		
							plateau[i][j].setImage(Carte.IMAGE_NAIN);		
							break;
						case ELF:
							plateau[i][j].setImage(Carte.IMAGE_ELF);
							break;
						case HUMAIN:
							plateau[i][j].setImage(Carte.IMAGE_HUMAIN);
							break;
						case HOBBIT:
							plateau[i][j].setImage(Carte.IMAGE_HOBBIT);
							break;
						}
					}*/
				}
			}
		}
		//System.out.println("resetT");

	}
	
	public void seDessinerDecouvert(Graphics g) {
		int i,j;
		for(i=0;i<HAUTEUR_CARTE;i++) { 
			for(j=0;j<LARGEUR_CARTE;j++) { 
				plateau[i][j].seDessiner(g); 
			} 			
		} 
	}
	

	public int finJeu() {
		System.out.println("dans fin jeu");
		if(Heros.nbHeros==0) {
			System.out.println("Défaite");
			return PanneauJeu.ORDI;
			
					
		}else if(Monstre.nbMonstre==0) {
			System.out.println("Victoire");
			return PanneauJeu.JOUEUR;
		}
		return 0;
	}

	public int getTour() {
		return tour;
	}

	public void setTour(int tour) {
		this.tour = tour;
	}
	
	
	
	
	
}