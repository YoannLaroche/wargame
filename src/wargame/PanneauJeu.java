package wargame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.JPanel;


public class PanneauJeu extends JPanel implements IConfig, Serializable{
	private static final long serialVersionUID = 1L;
	final static int JOUEUR = 1;
	final static int ORDI = 2;
	public static int gagnant= 0;
	public Carte carte;
	private Position souris=new Position(-1,-1);
	boolean bouge = false;
	Element HerosBouge ;
	Element direction;
	Hud menu;


	protected static Son MONSTREATTAQUE, MONSTREMORT, HEROSATTAQUE, HEROSMORT, HEROSBOUGE, HEROSFLECHE, MONSTREFLECHE, BOIRE;
		
	public PanneauJeu(){
		Dimension d = new Dimension(LARGEUR_CARTE*NB_PIX_CASE , HAUTEUR_CARTE*NB_PIX_CASE);
		setSize(d);
		setVisible(true);
		carte = new Carte();
		
		

		MONSTREMORT=new Son("son/mortmonstre.wav");
		MONSTREATTAQUE=new Son("son/monstreattaque.wav");
		HEROSBOUGE=new Son("son/bouge.wav");
		HEROSATTAQUE=new Son("son/herosattaque.wav");
		HEROSMORT=new Son("son/mortheros.wav");
		HEROSFLECHE=new Son("son/herosfleche.wav");
		MONSTREFLECHE=new Son("son/monstrefleche.wav");
		BOIRE=new Son("son/boire.wav");
		
		
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();
				int caseX = x/NB_PIX_CASE;//Recupere la case en x correspondant au clic souris
				int caseY = y/NB_PIX_CASE;//Recupere la case en Y correspondant au clic souris
			
				if(caseX>=0 && caseX < LARGEUR_CARTE && caseY>=0 && caseY < HAUTEUR_CARTE) {
					if(carte.plateau[caseY][caseX] instanceof Heros && ((Soldat)carte.plateau[caseY][caseX]).getAJoue()==false) {	
						bouge = true;
						HerosBouge = carte.plateau[caseY][caseX]; //new Heros (carte.plateau[caseY][caseX].getPosition(),((Heros)carte.plateau[caseY][caseX]).getSoldat());//Si c'est un heros on recupere le Heros en en question et on stock ses coordonnes et son type de soldat dans la variable HerosBouge
						direction = new Element(HerosBouge.getPosition());// et on simule son deplacement
					}
				}
			}
			
			public void mouseClicked(MouseEvent e) {
				if( carte.plateau[e.getY()/NB_PIX_CASE][e.getX()/NB_PIX_CASE] instanceof Soldat) {
					if (carte.plateau[e.getY()/NB_PIX_CASE][e.getX()/NB_PIX_CASE].getVisible()) {
						menu.soldat = (Soldat)carte.plateau[e.getY()/NB_PIX_CASE][e.getX()/NB_PIX_CASE];
						menu.repaint();
					}
				}
			}
			
			public void mouseReleased(MouseEvent e) {
				if(bouge) { //On verifie si on est en train de bouger un heros.
					bouge = false;
					if (carte.plateau[direction.getPosition().getY()/NB_PIX_CASE][direction.getPosition().getX()/NB_PIX_CASE] instanceof Monstre) { //Si on a bougé un heros sur un monstre on l'attaque
						((Heros)HerosBouge).combat(((Monstre)carte.plateau[direction.getPosition().getY()/NB_PIX_CASE][direction.getPosition().getX()/NB_PIX_CASE]));//Ok
						
						if(((Soldat)HerosBouge).getPoints()<=0) {
						    carte.mort((Soldat)HerosBouge);
						    HEROSMORT.jouerSon();
						}
						if(((Soldat)carte.plateau[direction.getPosition().getY()/NB_PIX_CASE][direction.getPosition().getX()/NB_PIX_CASE]).getPoints()<=0) {
							carte.mort((Soldat)carte.plateau[direction.getPosition().getY()/NB_PIX_CASE][direction.getPosition().getX()/NB_PIX_CASE]);
						    MONSTREMORT.jouerSon();
						}

						gagnant = carte.finJeu();
						((Soldat)HerosBouge).setAJoue(true);
						
					}	
					else if (!direction.getPosition().equals(HerosBouge.getPosition())) {// sinon on le bouge sur la case choisi si elle est differente de la position de depart
						
						carte.plateau[HerosBouge.getPosition().getY()/NB_PIX_CASE][HerosBouge.getPosition().getX()/NB_PIX_CASE] = new Element (HerosBouge.getPosition());
						HerosBouge.setPosition(direction.getPosition());
						carte.plateau[direction.getPosition().getY()/NB_PIX_CASE][direction.getPosition().getX()/NB_PIX_CASE] = HerosBouge;
						((Soldat)HerosBouge).setAJoue(true);
					
						HEROSBOUGE.jouerSon();
						
						repaint();
					}
					menu.repaint();
				}
			}
		});
		
		
		addMouseMotionListener(new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				souris.setX(e.getX());
				souris.setY(e.getY());
				//recupere les coordonnes de la case poité par la souris
				repaint();
			}

			public void mouseDragged(MouseEvent e)  {
				if (bouge) {
					int x = e.getX()/NB_PIX_CASE;
					int y = e.getY()/NB_PIX_CASE;
					if(x < LARGEUR_CARTE && y < HAUTEUR_CARTE && x >-1 && y > -1) { //on verifie si la case que l'on veut "dragged" est dans la carte
//   On verifie que la case soit :  1)         pleine       &                     soit un Monstre     &      qu'il est a portée du heros selectionné                                                                                                                                             2)  Ou       que l'on se deplace a une case de la position initial                                                                          et             la case et vide    ou  c'est la position de depart du heros                                                                                                
						/*if ((carte.plateau[y][x].getEtat() && carte.plateau[y][x] instanceof Monstre  && Math.abs(x-HerosBouge.getPosition().getX()/NB_PIX_CASE ) <= ((Heros)HerosBouge).getPortee() && Math.abs(y-HerosBouge.getPosition().getY()/NB_PIX_CASE ) <= ((Heros)HerosBouge).getPortee()) || (( Math.abs(x-HerosBouge.getPosition().getX()/NB_PIX_CASE ) <= 1 && Math.abs(y-HerosBouge.getPosition().getY()/NB_PIX_CASE ) <= 1 ) && (!carte.plateau[y][x].getEtat() || carte.plateau[y][x].getPosition().equals(HerosBouge.getPosition())))) {//ok
							direction = new Element (new Position(x*NB_PIX_CASE,y*NB_PIX_CASE));//On met a jour la position montrer quand on veut deplacer le heros
						}*/
						try {
							direction = carte.affecterPosition(HerosBouge, new Position(x,y));
						}catch(Exception ex){
							System.out.println(ex);
						}
					}
					repaint();
				}
			}
			
		});
		
		
	}
		
		
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		carte.toutDessiner(g);		
		/*affiche case sur laquelle la souris est*/
		if(gagnant == 0){
			if(bouge) {

			/*
			g.setColor(Color.black);
			if(HerosBouge.getPosition().estVoisine())
			g.drawRect(this.position.getX(),this.position.getY(),NB_PIX_CASE-1,NB_PIX_CASE-1);
			*/
			
				g.setColor(Color.red);
				g.drawRect(direction.getPosition().getX(), direction.getPosition().getY(), NB_PIX_CASE-1, NB_PIX_CASE-1);
			}
		
			if(souris.getX()/NB_PIX_CASE < LARGEUR_CARTE && souris.getY()/NB_PIX_CASE < HAUTEUR_CARTE) {
				if(carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE] instanceof Heros) {
					g.setColor(Color.black);
					if (souris.getX()/NB_PIX_CASE > LARGEUR_CARTE - 4)
					    g.drawString(((Heros)carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE]).toString(), LARGEUR_CARTE*NB_PIX_CASE - 3*NB_PIX_CASE, carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getY()); 
					else if (souris.getY()/NB_PIX_CASE < 1 )
						g.drawString(((Heros)carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE]).toString(), carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getX() , NB_PIX_CASE + 5);
					else
						g.drawString(((Heros)carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE]).toString(), carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getX() , carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getY());
				}
				if(carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE] instanceof Monstre && carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getVisible()) {
					g.setColor(Color.black);
					if (souris.getX()/NB_PIX_CASE > LARGEUR_CARTE - 4)
						g.drawString(((Monstre)carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE]).toString(), LARGEUR_CARTE*NB_PIX_CASE - 3*NB_PIX_CASE, carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getY()); 
					else if (souris.getY()/NB_PIX_CASE < 1 )
						g.drawString(((Monstre)carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE]).toString(), carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getX() , NB_PIX_CASE + 5);
					else
						g.drawString(((Monstre)carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE]).toString(), carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getX() , carte.plateau[souris.getY()/NB_PIX_CASE][souris.getX()/NB_PIX_CASE].getPosition().getY());
				}
			}
		}
		// Affichage du gagnant
		if(gagnant == JOUEUR) {
			carte.seDessinerDecouvert(g);
			g.setColor(new Color(0,0,0,80));
			g.fillRect(0,0 , LARGEUR_CARTE*NB_PIX_CASE, HAUTEUR_CARTE*NB_PIX_CASE);
			g.setColor(new Color(250,250,250));
			g.setFont(new Font("impact", Font.BOLD, 80));
			int largeur = g.getFontMetrics().stringWidth("WINNER");

						
			g.drawString("WINNER", ((LARGEUR_CARTE*NB_PIX_CASE)/2-largeur/2), ((HAUTEUR_CARTE*NB_PIX_CASE)/2));
		}
			
		if(gagnant == ORDI) {
			carte.seDessinerDecouvert(g);
			g.setColor(new Color(0,0,0,80));
			g.fillRect(0,0 , LARGEUR_CARTE*NB_PIX_CASE, HAUTEUR_CARTE*NB_PIX_CASE);
			g.setColor(new Color(250,250,250));
			g.setFont(new Font("impact", Font.BOLD, 70));
			int largeur = g.getFontMetrics().stringWidth("LOOSER");
					
			g.drawString("LOSER", ((LARGEUR_CARTE*NB_PIX_CASE)/2)-largeur/2, ((HAUTEUR_CARTE*NB_PIX_CASE)/2));
		}
	}	
}
