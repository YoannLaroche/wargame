package wargame;

import java.awt.Color;

public interface IConfig {
	int LARGEUR_CARTE = 35; int HAUTEUR_CARTE = 20; // en nombre de cases
	int LARGEUR_BULLE = 50; int HAUTEUR_BULLE = 100;
	int NB_PIX_CASE = 40;
	int ZONE_AFFICHAGE = 300; int BOUTTON = 50;
	int POSITION_X = 100; int POSITION_Y = 50; // Position de la fenêtre
	int NB_HEROS = 6; int NB_MONSTRES = 15; int NB_OBSTACLES = 50;
	Color COULEUR_VIDE = Color.white, COULEUR_INCONNU = Color.lightGray;
	Color COULEUR_TEXTE = Color.black, COULEUR_MONSTRES = Color.black;
	Color COULEUR_HEROS = Color.red, COULEUR_HEROS_DEJA_JOUE = Color.pink;
	Color COULEUR_EAU = Color.blue, COULEUR_FORET = Color.green, COULEUR_ROCHER = Color.gray;
	
	
	// macro menu
	public final static int MENU = 0;
	public final static int JEU = 1;
	
	// macro position des boutons
	int POSITIONX = 40;
	final static int POSITIONY = 60;
	final static int DIMX = 350 ;
	final static int DIMY = 50;
	final static int ECART = 20;
	final static int JESAISPAS = 30;
	
	// logo
	final static int LOGOY = 50;
}