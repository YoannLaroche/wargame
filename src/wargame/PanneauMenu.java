package wargame;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PanneauMenu extends JPanel implements IConfig{
	private static final long serialVersionUID = 1L;
	private Image fond;
	private Image logo;
	
	PanneauMenu() throws IOException {
		
		fond = ImageIO.read(this.getClass().getResource("img/fond_guerrier.jpg"));
		logo = ImageIO.read(this.getClass().getResource("img/wargame.png"));
	}
	
	PanneauMenu(int hauteur, int largeur){
		setSize(largeur, hauteur);
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(fond, 0, 0,POSITIONX*2+DIMX,DIMY*3+(POSITIONY+ECART)*2+JESAISPAS+LOGOY, null);
		g.drawImage(logo,POSITIONX, LOGOY, DIMX, DIMY,null);
	}
}