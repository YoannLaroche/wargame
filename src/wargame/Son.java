package wargame;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class Son {
	
	private URL url;
	private AudioClip clip;
	
	public Son(String file) {
		setUrl(PanneauJeu.class.getResource(file));
	}
	
	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}
	
	public AudioClip getClip() {
		return clip;
	}

	public void setClip(AudioClip clip) {
		this.clip = clip;
	}
	public void jouerSon() {
		if(clip!=null) {
			stopSon();
		}
		setClip(Applet.newAudioClip(url));
		clip.play();
	}
	
	public void stopSon() {
		clip.stop();
	}
	
}
