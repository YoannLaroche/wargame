package wargame;

import java.awt.Color;

import java.io.Serializable;

import java.awt.Graphics;
import java.awt.Image;


public class Element implements IConfig, Serializable {

	private static final long serialVersionUID = 1L;
	protected static java.awt.Color COULEUR_ROCHER = Color.gray;
	protected static java.awt.Color COULEUR_FORET = Color.green;
	protected static java.awt.Color COULEUR_EAU = Color.blue;
	
	public static final boolean VIDE = false;
	public static final boolean PLEIN = true;
	private Position position;
	private boolean etat = VIDE;
	private Color couleur = Color.WHITE;
	private boolean visible = false;
	
	//private Image image;
	
	public Element(Position position) {
		this.position = new Position (position.getX(),position.getY());
		
	}

	// getter
	public Position getPosition() {return this.position;}
	public boolean getEtat() {return this.etat;}
	public Color getCouleur() {return couleur;}
	public boolean getVisible() {return visible;}
	//public Image getImage() {return image; }
	
	//setter
	public void setPosition(Position position) {this.position = position;}
	public void setEtat(boolean etat) {this.etat = etat;}
	public void setCouleur(Color couleur) {this.couleur = couleur;}
	public void setVisible(boolean visible) {this.visible = visible;}
	/*public void setImage(String img){
	try {
		this.image=ImageIO.read(new File(img));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	};}*/
	
	Image getImage() {
		if(this instanceof Heros) {
			if(((Heros)this).getAJoue()) {
				switch(((Heros)this).getSoldat()) {
				case NAIN:
					return Carte.NAIN_NB;
				case ELF:
					return Carte.ELF_NB;
				case HUMAIN:
					return Carte.HUMAIN_NB;
				case HOBBIT:
					return Carte.HOBBIT_NB;
				}
			}else {
				switch(((Heros)this).getSoldat()) {
				case NAIN:
					return Carte.NAIN;
				case ELF:
					return Carte.ELF;
				case HUMAIN:
					return Carte.HUMAIN;
				case HOBBIT:
					return Carte.HOBBIT;
				}
			}
		}
		
		if(this instanceof Monstre) {
			switch(((Monstre)this).getSoldat()) {
			case ORC:
				return Carte.ORC;
			case GOBELIN:
				return Carte.GOBELIN;
			case TROLL:
				return Carte.TROLL;
			}
		}
		
		if(this instanceof Obstacle) {
			switch(((Obstacle)this).getType()) {
			case ROCHER:
				return Carte.ROCHER;
			case EAU:
				return Carte.EAU;
			case FORET:
				return Carte.FORET;
			}
		}
		return Carte.HERBE;
	}
	
	public void seDessiner(Graphics g) {
		g.setColor(this.getCouleur());
		//g.fillRect(this.position.getX(),this.position.getY(),NB_PIX_CASE,NB_PIX_CASE);

		g.drawImage(Carte.HERBE, this.position.getX(), this.position.getY(), NB_PIX_CASE, NB_PIX_CASE, null);
		g.drawImage(getImage(), this.position.getX(), this.position.getY(), NB_PIX_CASE, NB_PIX_CASE, null);
		/*
		g.setColor(Color.white);
		if (this instanceof Heros) {		
			g.setColor(Color.white);
			g.fillRect(this.position.getX(),this.position.getY(),NB_PIX_CASE,NB_PIX_CASE);
			//System.out.println(this.getImage());
			g.drawImage(this.image, this.position.getX(), this.position.getY(), NB_PIX_CASE, NB_PIX_CASE, null);
			//g.drawString(((Heros)this).stringNbHeros(), this.position.getX()+NB_PIX_CASE/4, this.position.getY()+3*NB_PIX_CASE/4);
		}
		
		if (this instanceof Monstre) {	
			g.setColor(Color.white);
			g.drawString(((Monstre)this).stringNbMonstre(), this.position.getX()+NB_PIX_CASE/4, this.position.getY()+3*NB_PIX_CASE/4);
		}		
		*/
		
		/*

		g.setColor(Color.black);
		g.drawRect(this.position.getX(),this.position.getY(),NB_PIX_CASE-1,NB_PIX_CASE-1);
		g.setColor(this.getCouleur());
		g.drawRect(0, 0, NB_PIX_CASE*LARGEUR_CARTE-1,NB_PIX_CASE*HAUTEUR_CARTE-1);
		*/
		
		this.visible  = true;
	}
	
	public void seDessinerCache(Graphics g) {
		/*g.setColor(Color.darkGray);
		g.fillRect(this.position.getX(),this.position.getY(),NB_PIX_CASE,NB_PIX_CASE);
		g.setColor(Color.black);
		g.drawRect(this.position.getX(),this.position.getY(),NB_PIX_CASE,NB_PIX_CASE);*/
		
		g.drawImage(Carte.BROUILLARD, this.position.getX(), this.position.getY(), NB_PIX_CASE, NB_PIX_CASE, null);
		
		this.visible = false;
	}
}
