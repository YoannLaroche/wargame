package wargame;

import java.awt.Color;

import java.io.Serializable;

public class Heros extends Soldat implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TypesH soldat;
	private Color couleur = Color.red; 
	public static int nbHeros = 0;
	private final int NUMERO;
	
	public Heros (Position position, TypesH heros) {
		super(position,heros.getPoints(),heros.getPortee(),heros.getPuissance(),heros.getTir(),0);
		this.soldat = heros;
		nbHeros++;
		NUMERO = nbHeros;		
	}
	
	
	
	public Color getCouleur() {
		return couleur;
	}

	public void setCouleur(Color couleur) {
		this.couleur= couleur;
	}
	
	public int getNumero() {
		return NUMERO;
	}
	
	public TypesH getSoldat() {
		return soldat;
	}
	
	public String stringNbHeros() {
		return String.valueOf(NUMERO);
	}
	
	public String toString() {
		return soldat+" "+NUMERO+" ("+this.getPoints()+"PV|"+soldat.getPoints()+"PV)";
	}
}
