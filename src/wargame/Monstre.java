package wargame;

import java.awt.Color;

import java.io.Serializable;

public class Monstre extends Soldat implements Serializable{

	private static final long serialVersionUID = 1L;
	public TypesM soldat;
	private Color couleur = Color.orange;
	public static int nbMonstre = 0;
	private final int NUMERO;
	
	public Monstre (Position position, TypesM monstre) {
		super(position,monstre.getPoints(),monstre.getPortee(),monstre.getPuissance(),monstre.getTir(),0);
		this.soldat = monstre;
		nbMonstre++;
		NUMERO = nbMonstre;
	}
	
	public Color getCouleur() {
		return couleur;
	}

	public TypesM getSoldat() {
		return soldat;
	}
	
	public int getNumero() {
		return NUMERO;
	}
	
	public String stringNbMonstre() {
		return String.valueOf(NUMERO);
	}
	
	public void deplacerMonstreAlea(Carte carte) {
		int posMonstreX = (int)(this.getPosition().getX()/NB_PIX_CASE);
		int posMonstreY = (int)(this.getPosition().getY()/NB_PIX_CASE);		
		
		int i;
		int j;
		do {
				i = posMonstreX+(int)(Math.random()*3)-1;
				j = posMonstreY+(int)(Math.random()*3)-1;
				//System.out.println("position "+i+" "+j);
		}while(i<0 || i>=LARGEUR_CARTE || j<0 || j>=HAUTEUR_CARTE || i == posMonstreY || j == posMonstreX || carte.plateau[j][i].getEtat());
		
		
		
		carte.plateau[j][i]=carte.plateau[posMonstreY][posMonstreX];
		

		carte.plateau[posMonstreY][posMonstreX]=new Element(carte.plateau[posMonstreY][posMonstreX].getPosition());
		carte.plateau[posMonstreY][posMonstreX].setEtat(false);
		
		carte.plateau[j][i].setPosition(new Position(i*NB_PIX_CASE, j*NB_PIX_CASE));
		((Monstre) carte.plateau[j][i]).setAJoue(true);
		
	}
	
	public void tourMonstre(Carte carte) {
		
		// si le monstre a moins de 1/4 de vie il se regenere
		if(this.getPoints() < soldat.getPoints()/4 && this.getPoints() < soldat.getPoints()) {
			this.setPoints(this.getPoints()+1);
		}
		// sinon il attaque un monstre a proximite ou se deplace
		else {
			
			Heros cible = carte.trouveHeros(this.getPosition());
			if(cible != null) {
				System.out.println("x = "+this.getPosition().getX()+" y = "+this.getPosition().getY()+"trouve hero");
				this.combat(cible);
				
				if(((Soldat)cible).getPoints()<=0) {
					carte.mort((Soldat)cible);
					PanneauJeu.HEROSMORT.jouerSon();
				}
				if(((Soldat)carte.plateau[this.getPosition().getY()/NB_PIX_CASE][this.getPosition().getX()/NB_PIX_CASE]).getPoints()<=0) {
					carte.mort((Soldat)carte.plateau[this.getPosition().getY()/NB_PIX_CASE][this.getPosition().getX()/NB_PIX_CASE]);
					PanneauJeu.MONSTREMORT.jouerSon();
				}
				
				PanneauJeu.gagnant = carte.finJeu();
			}
			else {
				deplacerMonstreAlea(carte);
			}
		}
		this.tourSuivant();
	}
	
	public String toString() {
		return soldat+" "+NUMERO+" ("+this.getPoints()+"PV|"+soldat.getPoints()+"PV)";

	}

}
