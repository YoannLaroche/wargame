package wargame;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Hud extends JPanel implements IConfig{

	private static final long serialVersionUID = 1L;
	private static Image HUD;
	Soldat soldat;
	JLabel info;
	JLabel pv ;
	JLabel puissance;
	JLabel tir;
	JLabel nom;
	public Hud() {
		try {
			HUD = ImageIO.read(this.getClass().getResource("img/papyrus.jpeg"));
		}catch(Exception e) {
		e.printStackTrace();
		}
		setLayout(null);
		setPreferredSize(new Dimension(ZONE_AFFICHAGE,HAUTEUR_CARTE*NB_PIX_CASE));
		
		info = new JLabel("Il reste "+Heros.nbHeros+" héros et "+Monstre.nbMonstre+" monstres");
		info.setHorizontalAlignment(JLabel.CENTER);
		info.setBorder(BorderFactory.createLoweredBevelBorder());
		info.setBounds(5, 130, ZONE_AFFICHAGE - 10 , 20);
		info.setOpaque(true);
		info.setFont(new java.awt.Font(Font.SERIF,Font.ITALIC,15));
		add(info);
		nom = new JLabel();
		nom.setBounds(50,330,200,30);
		nom.setHorizontalAlignment(JLabel.CENTER);
		add(nom);
		pv = new JLabel ();
		pv.setBounds(50,360,200,30);
		pv.setHorizontalAlignment(JLabel.CENTER);
		add(pv);
		puissance = new JLabel();
		puissance.setBounds(50,390,200,30);
		puissance.setHorizontalAlignment(JLabel.CENTER);
		add(puissance);
		tir = new JLabel();
		tir.setBounds(50,420,200,30);
		tir.setHorizontalAlignment(JLabel.CENTER);
		add(tir);
	}
	
	
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(HUD,0,0,ZONE_AFFICHAGE,HAUTEUR_CARTE*NB_PIX_CASE,null);
		info.setText("Il reste "+Heros.nbHeros+" héros et "+Monstre.nbMonstre+" monstres");
		if (soldat != null) {
			g.drawImage(soldat.getImage(),50,150,null);
			nom.setVisible(true);
			if ( soldat instanceof Heros) {
				System.out.println("oui");
				nom.setText(((Heros)soldat).getSoldat() + " " + ((Heros)soldat).getNumero());
			}
			else if ( soldat instanceof Monstre)
				nom.setText(((Monstre)soldat).getSoldat() + " " + ((Monstre)soldat).getNumero());
			pv.setVisible(true);
			pv.setText("Points de vie : "+soldat.getPoints());
			puissance.setVisible(true);
			puissance.setText("Puissance de frappe : "+ soldat.getPuissance());
			tir.setVisible(true);
			tir.setText("Puissance de tir : "+ soldat.getTir());
		}
	}
}
