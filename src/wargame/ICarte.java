package wargame;

import java.awt.Graphics;

public interface ICarte {
	Element getElement(Position pos);
	Heros trouveHeros(Position pos); // Trouve un héros choisi aléatoirement
									 // parmi les 8 positions adjacentes de pos
	void mort(Soldat perso);
	void toutDessiner(Graphics g);
}