package wargame;

import java.awt.Color;

import java.io.Serializable;

public class Obstacle extends Element implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum TypeObstacle {
		ROCHER (COULEUR_ROCHER), FORET (COULEUR_FORET), EAU (COULEUR_EAU);
		
		private final Color COULEUR;
		
		TypeObstacle(Color couleur) { COULEUR = couleur; }
		
		public static TypeObstacle getObstacleAlea() {
			return values()[(int)(Math.random()*values().length)];
		}
	}
	private TypeObstacle TYPE;
	
	Obstacle(TypeObstacle type, Position pos) { 
		super(pos);
		TYPE = type; 
		this.setPosition(pos);
		
	}
	
	public String toString() { return ""+TYPE; }
	
	public Color getCouleur() {
		return TYPE.COULEUR;
	}
	
	public TypeObstacle getType() {
		return TYPE;
	}
	
}